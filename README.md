This project is a walkthrough of a [tutorial](https://web-crunch.com/posts/ruby-on-rails-api-vue-js) about RoR and Vue.js

**Objective:**
1. Gain a deeper understanding of the technology stack that GitLab is built on.

*as measured by...*

**Key Results:**
1. Finish the tutorial by tracking progress using GitLab's project planning capabilities
1. Use GitLab as a repository
1. Deploy the app to Heroku using GitLab CI
1. Submit five issues with feedback about the end-to-end user experience
